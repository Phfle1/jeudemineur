'use strict'

/**
 * Représente un jeu de démineur en mémoire, soit un tableau à deux dimensions où
 * chaque case contient une mine ou un nombre. Le nombre représente le nombre de mines
 * autour.
 *
 * @class JeuDemineur
 */
class JeuDemineur {
  constructor (nbMines, nbRangees, nbColonnes) {
    this.VerifierParametres(nbRangees, nbColonnes, nbMines)
    this.nbColonnes = nbColonnes
    this.nbMines = nbMines
    this.nbRangees = nbRangees
    this.carMine = -1
    this.GenererJeu()
  }

  /**
   *Vérifie les trois paramètres et lance des exceptions si ils sont erronés.
   *nbRangees > 0
   *nbColonnes > 0
   *nbMines > 0 et < (nbRangees * nbColonnes + 1)
   * @param {*} nbRangees
   * @param {*} nbColonnes
   * @param {*} nbMines
   * @memberof JeuDemineur
   */
  VerifierParametres (nbRangees, nbColonnes, nbMines) {
    if (isNaN(nbRangees) || isNaN(nbColonnes) || nbColonnes <= 0 || nbRangees <= 0) {
      throw new TypeError('les paramètres nbRangees et nbColonnes doivent être des nombres supérieurs à 0')
    }
    const nbCase = nbColonnes * nbRangees
    if (isNaN(nbMines) || nbCase <= nbMines || nbMines <= 0) {
      throw new TypeError('Le nombre de mines doit être un nombre supérieur à 0 et ne doit pas être supérieur au nombre de cases + 1')
    }
  }

  /**
   * créé le jeu en fonction des paramètres et remplit
   * chaque case avec une mine ou un nombre
   *
   * @memberof JeuDemineur
   */
  GenererJeu () {
    this.CreerTableauVide()
    this.GenererCasesMines()
    this.GenererNombres()
  }

  /**
   * Crée un tableau de tableau dont chaque case a la valeur 0 en fonction de
   * du nombre de colonnes et de rangées du jeu.
   *
   * @memberof JeuDemineur
   */
  CreerTableauVide () {
    this.tableau = new Array(this.nbRangees)
    for (let i = 0; i < this.nbRangees; i++) {
      this.tableau[i] = new Array(this.nbColonnes)
      for (let j = 0; j < this.nbColonnes; j++) {
        this.tableau[i][j] = 0
      }
    }
  }

  /**
   *Pour chaque mine dans le jeu, ajoute 1 dans les cases autour.
   *
   * @memberof JeuDemineur
   */
  GenererNombres () {
    for (let i = 0; i < this.nbRangees; i++) {
      for (let j = 0; j < this.nbColonnes; j++) {
        if (this.tableau[i][j] === this.carMine) {
          this.AjouterUnAutour(i, j)
        }
      }
    }
  }

  /**
   *Ajouter 1 à toutes les cases autour de la case ayant les coordonnées (colonne, range)
   *sauf pour les cases contenant une mines
   * @param {Number} colonne
   * @param {Number} rangee
   * @memberof JeuDemineur
   */
  AjouterUnAutour (colonne, rangee) {
    const indexHorizontalMin = (rangee === 0) ? 0 : rangee - 1
    const indexVerticalMin = (colonne === 0) ? 0 : colonne - 1
    const indexHorizontalMax = (rangee === this.nbColonnes - 1) ? this.nbColonnes - 1 : rangee + 1
    const indexVerticalMax = (colonne === this.nbRangees - 1) ? this.nbRangees - 1 : colonne + 1
    for (let i = indexVerticalMin; i <= indexVerticalMax; i++) {
      for (let j = indexHorizontalMin; j <= indexHorizontalMax; j++) {
        const courant = this.tableau[i][j]
        if (courant !== this.carMine) {
          this.tableau[i][j] = courant + 1
        }
      }
    }
  }

  /**
   *Permet d'obtenir le contenu d'une case du jeu en fonction de ses coordonnées (colonne, rangee)
   *
   * @param {*} colonne
   * @param {*} rangee
   * @return {Number} un entier entre -1 et 8. -1 étant une mine, les entiers de 0 à 8 représentent le nombre
   *de mines autour de la case.
   * @memberof JeuDemineur
   */
  ContenuCase (colonne, rangee) {
    return this.tableau[rangee][colonne]
  }

  /**
   *Ajoute des mines dans le jeu
   *
   * @memberof JeuDemineur
   */
  GenererCasesMines () {
    let nbMinesPlacees = 0
    while (nbMinesPlacees < this.nbMines) {
      const colonne = Math.floor(Math.random() * this.nbColonnes)
      const rangee = Math.floor(Math.random() * this.nbRangees)
      const contenuCase = this.tableau[rangee][colonne]
      if (contenuCase !== this.carMine) {
        this.tableau[rangee][colonne] = this.carMine
        nbMinesPlacees++
      }
    }
  }
}

// Utilisation
// const jeu = new JeuDemineur(2, 4, 5)
// console.log(jeu)
// console.log(jeu.ContenuCase(0, 0))
